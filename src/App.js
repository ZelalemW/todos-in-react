import React, { useState, useRef, useEffect } from "react";
import TodoList from "./TodoList";
import "./lib/bootstrap.min.css";
import { v4 as uuidv4 } from "uuid";

function App() {
  const TODOS_KEY = "MY_ACTION_ITEMS";

  // destructuring.
  const [todos, setTodos] = useState([]);
  const todoNameRef = useRef();
  const style = {
    marginTop: 50,
  };

  //because of [] array, this runs just when the page loads
  useEffect(() => {
    const storedTodos = JSON.parse(localStorage.getItem(TODOS_KEY));
    setTodos(storedTodos);
  }, []);

  useEffect(() => {
    if (todos) {
      localStorage.setItem(TODOS_KEY, JSON.stringify(todos));
    }
  }, [todos]);

  const toggleHandler = (id) => {
    const newItems = [...todos];
    const targetTodo = newItems.find((x) => x.id === id);
    targetTodo.complete = !targetTodo.complete;
    setTodos(newItems);
  };

  const deleteHandler = (id) => {
    const prevTodos = [...todos];
    const newTodos = prevTodos.filter((x) => x.id !== id);
    setTodos(newTodos);
  };

  const addTodoHandler = () => {
    const name = todoNameRef.current.value;
    if (name === "") {
      return;
    }

    setTodos((prevTodos) => [
      ...prevTodos,
      { id: uuidv4(), name: name, complete: false },
    ]);
    todoNameRef.current.value = null; // clear it afterwards
  };

  const clearInputHandler = () => {
    todoNameRef.current.value = null; // clear it afterwards
  };

  const clearCompletedHandler = () => {
    const newTodos = todos.filter((x) => !x.complete);
    setTodos(newTodos);
  };

  const clearAlldHandler = () => {
    setTodos([]);
    todoNameRef.current.value = null; // clear it afterwards
  };
  return (
    <div style={style}>
      <div className="card">
        <div className="card-header">My todos</div>
        <div className="card-body">
          <h5 className="card-title">Summary</h5>
          <div>{todos.filter((t) => !t.complete).length} left to do</div>
          <div>{todos.filter((t) => t.complete).length} completed</div>
          <hr />
          <div className="input-group">
            <input
              type="text"
              ref={todoNameRef}
              className="form-control"
              placeholder="Please enter your todo here..."
            />
            <br />
            <div className="input-group-append">
              <button className="btn btn-primary" onClick={addTodoHandler}>
                Add
              </button>
              <button className="btn btn-warning" onClick={clearInputHandler}>
                Clear Input
              </button>
              <button
                className="btn btn-secondary"
                onClick={clearCompletedHandler}
              >
                Clear Completed
              </button>
              <button className="btn btn-danger" onClick={clearAlldHandler}>
                Clear All
              </button>
            </div>
          </div>
          <br />
          <TodoList
            todos={todos}
            onToggle={toggleHandler}
            onDelete={deleteHandler}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
