import React from "react";
import "./lib/bootstrap.min.css";

export default function ToDo({ todo, onToggle, onDelete}) {
  return (
    <div>
      <div className="input-group mb-3">
        <div className="input-group-prepend">
          <div className="input-group-text">
            <input
              type="checkbox"
              checked={todo.complete}
              onChange={()=>onToggle(todo.id)}
            />
          </div>
        </div>
        <label className="form-control"> {todo.name}</label>
        <button className="btn btn-danger" onClick = { ()=>onDelete(todo.id)}>Delete</button>
      </div>
    </div>
  );
}
