import React from "react";
import Todo from './Todo'

export default function TodoList({todos, onToggle, onDelete}) {
  return todos.map(w => {
    return <Todo key={w.id} todo={w} onToggle={onToggle} onDelete={onDelete}/>;
  });
}
